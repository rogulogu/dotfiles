;;; custom-settings.el --- Emacs custom settings
;;; Commentary:
;;; Code:

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(desktop-save-mode t)
 '(ecb-options-version "2.40")
 '(indent-tabs-mode nil)
 '(js-indent-level 2)
 '(kill-whole-line t)
 '(package-archive-priorities '(("melpa" . 2)))
 '(package-archives
   '(("gnu" . "https://elpa.gnu.org/packages/")
     ("melpa" . "https://melpa.org/packages/")
     ("nongnu" . "https://elpa.nongnu.org/nongnu/")))
 '(package-enable-at-startup nil)
 '(package-selected-packages
   '(js2-mode json-mode auto-compile use-package gradle-mode groovy-mode org python go-mode avy-flyspell virtualenvwrapper dart-mode flycheck yaml-mode toml-mode markdown-mode magit avy-flycheck alchemist))
 '(safe-local-variable-values
   '((buffer-file-coding-system . utf-8-unix)
     (encoding . utf-8)
     (cperl-use-v6 . t)
     (cperl-indent-level . 4)))
 '(save-place t nil (saveplace))
 '(scroll-bar-mode 'right)
 '(select-enable-clipboard t)
 '(send-mail-function 'sendmail-send-it)
 '(sgml-validate-command "onsgmls -s")
 '(sh-basic-offset 2)
 '(show-paren-mode t)
 '(size-indication-mode t)
 '(tool-bar-mode nil)
 '(uniquify-buffer-name-style 'forward nil (uniquify)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;;; custom-settings.el ends here
