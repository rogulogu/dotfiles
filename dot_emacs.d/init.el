;;; init.el --- Emacs init file

;;; Commentary:

;;; Code:

;; start up

(package-initialize)

;; additional load paths
(add-to-list 'load-path "~/.emacs.d/elisp")

;; custom settings
(setq custom-file "~/.emacs.d/custom-settings.el")
(load custom-file)

;; use-package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

(setq use-package-verbose t)
(setq use-package-always-ensure t)

;; start Emacs server
(server-start)

;; stash backup files in folder in directory
(setq backup-directory-alist '(("." . "~/.emacs.d/backups")))

;; Packages

(use-package auto-compile
  :config (auto-compile-on-load-mode))
(setq load-prefer-newer t)

;; on the fly syntax checking

(use-package flycheck
  :defer 1
  :hook (after-init . global-flycheck-mode))

(use-package avy-flycheck
  :defer 1
  :config (avy-flycheck-setup))
  
;; Git interface

(use-package magit
  :bind ([f8] . magit-status))

;; JSON text format

(use-package json-mode
  :mode "\\.json\\'")

;; Markdown text format

(use-package markdown-mode
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)))

;; TOML text format

(use-package toml-mode
  :mode "\\.toml\\'")

;; YAML text format
(use-package yaml-mode
  :mode "\\.ya?ml\\'")

;; Dart

(use-package dart-mode
  :mode "\\.dart\\'")

;; Elixir

(use-package alchemist
  :defer 1)

(use-package elixir-mode
  :mode (("\\.exs\\'" . elixir-mode)
         ("\\.ex\\'" . elixir-mode)))
;; Go

(use-package go-mode
  :hook ((before-save . gofmt-before-save)
         (go-mode . (lambda () (set (make-local-variable 'compile-command) "go "))))
  :mode "\\.go\\'")

;; Gradle build

(use-package gradle-mode
  :defer t
  :requires s)

;; Groovy

(use-package groovy-mode
  :defer t
  :requires (dash s))

;; Javascript

(use-package js2-mode
  :mode "\\.js\\'")

;; Perl

(defalias 'perl-mode 'cperl-mode)

;; Python

(use-package python
  :mode ("\\.py\\'" . python-mode)
  :interpreter ("python" . python-mode))

(use-package virtualenvwrapper
  :config (venv-initialize-interactive-shells)
  :requires (dash s))


;; Function keys

(global-set-key [f2] 'rename-buffer)
(global-set-key [f3] 'comment-region)
(global-set-key [(shift f3)] 'uncomment-region)
(global-set-key [f4] 'indent-region)
(global-set-key [(shift f4)] 'join-line)
(global-set-key [f5] 'auto-revert-mode)
(global-set-key [(shift f5)] 'auto-revert-tail-mode)
;(global-set-key [f8] 'magit-status)
(global-set-key [f9] 'compile)
(global-set-key [(shift f9)] '(lambda () (interactive) (compile compile-command t)))


;;; init.el ends here
